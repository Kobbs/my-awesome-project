#include <stdio.h>
#include <stdlib.h>
#include "defines.h"

void findshape2(int mtrx[C][R]);
void findshape(int mtrx[C][R]);

void findshape2(int mtrx[C][R])
//get the shapes outlined with -1 (above + below)
{
	DECMATRIX
	int flag = 1, save;

	ENTERMATRIX(C,R)
	if(j < R-1 && mtrx[i][j] != mtrx[i][j+1]){
		if (flag){
			mtrx[i][j] = -1;
			flag = 0;
		} else {
			save = mtrx[i][j+1];
			mtrx[i][++j] = -1;
			flag = 1;
			if(save != mtrx[i][j+1])
				flag = 0;
		}	
	}
	ENTERLIMBO
	flag = 1;
	EXITLIMBO
}

void findshape(int mtrx[C][R])
//get the shapes outlined with -1 (above + below)
{
	DECMATRIX
	int flag = 1, save;

	CROSSMATRIX(C,R)
	if(i < C-1 && mtrx[i][j] != mtrx[i+1][j]){
		if(flag) {
			mtrx[i][j] = -1;
			flag = 0;
		} else { 
			save = mtrx[i+1][j];
			mtrx[++i][j] = -1;
			flag = 1;
			if(save != mtrx[i+1][j])
				flag = 0;
		}
	} 
	ENTERLIMBO
	flag = 1;
	EXITLIMBO
}
