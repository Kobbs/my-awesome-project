#define DECMATRIX int i=0, j=0;
#define ENTERMATRIX(a,b) for(i=0; i<a; i++){ for(j=0; j<b; j++){
#define CROSSMATRIX(b,a) for(j=0; j<a; j++){ for(i=0; i<b; i++){
#define EXITMATRIX }}
#define STARTLIMBO(a) for(i=0; i<a; i++){
#define ENTERLIMBO }
#define EXITLIMBO }
#define C 23
#define R 80

enum {RIGHT, LEFT, UP, DOWN};

struct clist{
	int i, j;
	struct clist *link;
};

void findshape(int mtrx[C][R]);
void findshape2(int mtrx[C][R]);
void creatematrix(int mtrx[C][R]);
void output(int mtrx[C][R]);
void findcorner(int mtrx[C][R], int tomtrx[C][R]);
void matrixpathing(int mtrx[C][R]);
