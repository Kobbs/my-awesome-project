#include <stdio.h>
#include <stdlib.h>
#include "defines.h"

static int iscorner(int mtrx[C][R], int i, int j);
static int outcorner(int mtrx[C][R], int i, int j);
static int incorner(int mtrx[C][R], int i, int j);

void findcorner(int mtrx[C][R], int tomtrx[C][R])
//find corner pieces
{
	DECMATRIX

	ENTERMATRIX(C,R)
	if(iscorner(mtrx, i, j))
		tomtrx[i][j] = 1;
	else
		tomtrx[i][j] = -1;
	EXITMATRIX
}

static int iscorner(int mtrx[C][R], int i, int j)
// test pieces around i,j to see if its a corner
{
	if(mtrx[i][j] == -1)
		return incorner(mtrx, i, j);
	else
		return outcorner(mtrx, i, j);
 
}

static int incorner(int mtrx[C][R], int i, int j)
//checks squares around i, j to see if corner
{
	if(mtrx[i][j+1] != -1 && mtrx[i+1][j] != -1)
		return 1;
	if(mtrx[i][j-1] != -1 && mtrx[i+1][j] != -1)
		return 1;
	if(mtrx[i][j+1] != -1 && mtrx[i-1][j] != -1)
		return 1;
	if(mtrx[i][j-1] != -1 && mtrx[i-1][j] != -1)
		return 1;
	return 0;
}

static int outcorner(int mtrx[C][R], int i, int j)
//checks squares around i, j to see if corner
{
	if(mtrx[i][j+1] == -1 && mtrx[i+1][j] == -1 &&
			(i == 0 || mtrx[i-1][j+1] == mtrx[i][j]) &&
			(j == 0 || mtrx[i+1][j-1] == mtrx[i][j]))
		return 1;
	if(mtrx[i][j-1] == -1 && mtrx[i+1][j] == -1 &&
			(i == 0 || mtrx[i-1][j-1] == mtrx[i][j]) &&
			(j == R-1 || mtrx[i+1][j+1] == mtrx[i][j]))
		return 1;
	if(mtrx[i][j+1] == -1 && mtrx[i-1][j] == -1 &&
			(j == 0 || mtrx[i-1][j-1] == mtrx[i][j]) &&
			(i == C-1 || mtrx[i+1][j+1] == mtrx[i][j]))
		return 1;
	if(mtrx[i][j-1] == -1 && mtrx[i-1][j] == -1 &&
			(j == R-1 || mtrx[i-1][j+1] == mtrx[i][j]) &&
			(i == C-1 || mtrx[i+1][j-1] == mtrx[i][j]))
		return 1;
	return 0;
}
