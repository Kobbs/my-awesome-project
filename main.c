#include <stdlib.h>
#include <stdio.h>

#include "defines.h"

void combine(int mtrxA[C][R],int mtrxB[C][R],int mtrx[C][R]);
void combine2(int mtrxA[C][R],int mtrxB[C][R],int mtrx[C][R]);
void zero(int mtrx[C][R]);

//buggy when shapes are too close to eachother or bounds

int main(int argc, char** argv)
//much confusion awaits
{
	int mtrx[C][R], mtrxA[C][R], mtrxB[C][R], mtrxC[C][R],
		mtrxD[C][R];

	creatematrix(mtrxA);
	creatematrix(mtrxB);
	findshape(mtrxA);
	findshape2(mtrxB);
	combine(mtrxA, mtrxB, mtrxD);
	findcorner(mtrxD, mtrxC);
	combine2(mtrxA, mtrxB, mtrx);
	findcorner(mtrx, mtrxA);
	combine(mtrxC, mtrxA, mtrx);
	combine2(mtrx, mtrxD, mtrxB);
	combine2(mtrx, mtrxB, mtrxC);
	combine(mtrxB, mtrxC, mtrx);
	zero(mtrx);
	output(mtrx);
	
}

void zero(int mtrx[C][R])
{
	DECMATRIX

	ENTERMATRIX(C,R)
	if (mtrx[i][j] == -1)
		mtrx[i][j] = 1;
	else
		mtrx[i][j] = 0;
	EXITMATRIX
}

void combine2(int mtrxA[C][R],int mtrxB[C][R],int mtrx[C][R])
//combine A and B into thrid matrix
{
	DECMATRIX

	ENTERMATRIX(C,R)
	if(mtrxA[i][j] != mtrxB[i][j])
		mtrx[i][j] = 1;
	else
		mtrx[i][j] = -1;
	EXITMATRIX
}

void combine(int mtrxA[C][R],int mtrxB[C][R],int mtrx[C][R])
//combine A and B into thrid matrix
{
	DECMATRIX

	ENTERMATRIX(C,R)
	if(mtrxA[i][j] == mtrxB[i][j])
		mtrx[i][j] = mtrxA[i][j];
	else
		mtrx[i][j] = -1;
	EXITMATRIX
}

void creatematrix(int mtrx[C][R])
//create initial cross matrix //change numbers here to understand alg.
{
	DECMATRIX

	ENTERMATRIX(C,R)
	if((i<C-21 || i>C-2) || (j<R-43 || j>R-16))
		mtrx[i][j] = 1; //border of 2
	else if((i<C-17 || i>C-14) && (j<R-33 || j>R-25))
		mtrx[i][j] = 1; //squares around cross
	else 
		mtrx[i][j] = 0; //cross
	if ((j >= R-30 && j <= R-28) && (i < C-2 && i > C-21))
		mtrx[i][j] = 1;
	if ((j >= R-34 && j <= R-25) && (i < C-14 && i > C-17))
		mtrx[i][j] = 2;
	EXITMATRIX
}

void output(int mtrx[C][R])
//output each matrix in the linked list array
{
	DECMATRIX

	ENTERMATRIX(C,R)
	if(mtrx[i][j] == -1)
		printf(" ");
	else
		printf("%i", mtrx[i][j]);
	ENTERLIMBO
	printf("\n");
	EXITLIMBO
}
